get_attachments <- function(fp_id, dest) {

  python.exec("from exchangelib import DELEGATE, Account, Credentials, Configuration")
  python.exec(sprintf("credentials = Credentials(username='ADS\\\\%s', password='%s')", Sys.info()['user'], mmge:::iup()))
  python.exec("config = Configuration(server='eas.exchange.iu.edu', credentials=credentials)")
  python.exec(sprintf("account = Account(primary_smtp_address='%s@iu.edu', config=config, autodiscover=False, access_type=DELEGATE)", Sys.info()['user']))
  python.exec(sprintf("

  xx = account.inbox.filter(\"subject in 'ISSUE=%s PROJ=161'\")

  for item in xx:
    for attachment in item.attachments:
      local_path = '/media/R/outlook/' + attachment.name
      with open(local_path, 'wb') as f:
        f.write(attachment.content)
        print('saved attachment to ', local_path)

  ", fp_id))

}