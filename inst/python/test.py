from exchangelib import DELEGATE, Account, Credentials, Configuration

credentials = Credentials(username='ADS\\baileye', password='This is not a test!')

config = Configuration(server='eas.exchange.iu.edu', credentials=credentials)

account = Account(
    primary_smtp_address='baileye@iu.edu',
    config=config,
    autodiscover=False,
    access_type=DELEGATE)

# Print first 100 inbox messages in reverse order
for item in account.inbox.all().order_by('-datetime_received')[:100]:
    print(item.subject, item.body, item.attachments)
