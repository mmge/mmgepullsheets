#'Write pullsheets to the working directory
#'
#'Wrapper for xlsx::write.xlsx that sets common defaults and formats file name
#'in standard way
#'
#'@param df The pullsheet to write
#'@param filename The base filename to use
#'@param sheetName The sheetname for the file to write
#'@param \dots Any additional arguments to pass to write.xlsx
#'@param file_name Optional filename to save the file. Ignores configuration if this is present.
#'
#'If you are writing multiple sheets be sure to pass the \code{append = TRUE}
#'argument.
#'
#'@export
write_pullsheet <- function(pullsheet, ..., dir = ".", file_name) {

  if(missing(file_name)) {
    config <- get_pullsheet_config(dir)
    save_file <- file.path(config$directory, "output", sprintf("%s_%s.xlsx", config$order_name, Sys.Date()))
  } else {
    save_file = file_name
  }

  if(inherits(pullsheet, "data.frame")) {
    pullsheet <- list("Pullsheet" = pullsheet)
  }

  if(!inherits(pullsheet, "list")) {
    stop("Incompatible data type for pullsheet must be a data.frame or a list of data.frames", call. = FALSE)
  }

  for(s in seq_along(pullsheet)) {

    df <- pullsheet[[s]]

    if(!inherits(df, "data.frame")) {
      stop(names(pullsheet)[s], " is not a data.frame.", call. = FALSE)
    }

    append <- s > 1

    df %>%
      as.data.frame() %>%
      xlsx::write.xlsx(file = save_file,
                       sheetName = names(pullsheet)[s],
                       row.names = FALSE,
                       append = append,
                       ...
                    )

  }

  return(save_file)

}