#'@export
oncore_pullsheet_query <- function(dir = ".") {

  config <- get_pullsheet_config(dir)

  fields <- c("SPECIMEN_NO", "SPECIMEN_STORAGE_LOCATION", "PARENT_SPECIMEN_ID",
              config$subject_id, "COLLECTION_GROUP", "SPECIMEN_TYPE",
              "VISIT", "SPECIMEN_QUANTITY", "SPECIMEN_BAR_CODE", "SPECIMEN_STATUS",
              "PCS_SPECIMEN_ID", "SPECIMEN_COMMENTS")

  if(!is.null(config$extra_fields)) {
    fields <- unique(c(fields, config$extra_fields))
  }

  query_name <- sprintf("%s_query", config$order_name)

  queryObject <- oncore_connect(verbose = config$verbose, env = config$oncore_environment, query_name = query_name) %>%
    select_fields_(.dots = fields) %>%
    filter_oncore(protocol(config$protocol))

  if(!is.null(config$annotations)) {
    queryObject <- queryObject %>%
      select_annotations(.dots = config$annotations)
  }

  if(config$only_available_specimens) {
    queryObject <- oncore2::filter_oncore(queryObject, oncore2::specimen_status("Available"))
  } else {
    queryObject <- oncore2::select_fields(queryObject, REASON_DESTROYED)
  }

  if(!is.null(config$collection_group)) {

    if("DNA" %in% config$collection_group) {
      config$collection_group <- unique(c(config$collection_group, "BUFFY COAT"))
    }

    cg <- paste0("or(", paste(sprintf("collection_group('%s')", config$collection_group), collapse = ", "),")")

    eval(parse(text = paste0("queryObject <- queryObject %>% filter_oncore(", cg, ")")))

  }

  return(queryObject)

}
