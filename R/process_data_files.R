#' Automatically read in xlsx and csv files
#'
#'Primarily meant to streamline pullsheet projects by automating the inclusion
#'of support data into the global environment
#'
#'@param dir the directory to read from
#'
#'@export
process_data_files <- function(dir = "data") {

  data_files <- list.files(dir)

  data_files <- data_files[!grepl("^~", data_files)] # Filter out Excel temp files

  if(length(data_files) > 0) {
    for(df in data_files) {
      vn <- make.names(gsub(".xlsx|.csv", "", df))
      if(grepl("csv$", df)) {
        eval(parse(text = sprintf("x <- read.csv('data/%s', stringsAsFactors = FALSE)", df)))
        assign(vn, x, envir = .GlobalEnv)
      } else {
        wb <- loadWorkbook(sprintf("data/%s", df))
        sheets <- getSheets(wb)
        if(length(sheets) == 1) {
          eval(parse(text = sprintf("x <- read.xlsx('data/%s', sheetIndex = 1, stringsAsFactors = FALSE)", df)))
          assign(vn, x, envir = .GlobalEnv)
        } else {
          for(sn in names(sheets)) {
            vn <- make.names(sn)
            eval(parse(text = sprintf("x <- read.xlsx('data/%s', sheetName = '%s', stringsAsFactors = FALSE)", df, sn)))
            assign(vn, x, envir = .GlobalEnv)
          }
        }
      }
    }
  }

}