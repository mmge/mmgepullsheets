#'@param order_id The order id from the API
#'@param api_name The name of the API to access
#'@param verbose Should there be console output?
#'@param exclude A list of files or dataframes containing specimen_nos that should be ignored
#'@param remove A list of sample_ids that should be removed from the request (An annoying hack because DMR cannot resubmit a request?)
#'@export
create_api_pullsheet <- function(order_id,
                                 api_name = "biosend",
                                 dna_aliquots = TRUE,
                                 verbose = FALSE,
                                 exclude,
                                 remove = NULL) {

  library(IUGBAPI)

  IUGBAPI::loadAPI(api_name)

  order <- IUGBAPI::getOrder(order_id)$samples

  ids <- sapply(order, function(x) {return(x$number)})
  names(ids) <- sapply(order, function(x) {return(x$sampleId)})

  ids <- ids[!names(ids) %in% remove]

  mmge::load_protocol_settings(api_name)

  pull <- data.frame(stringsAsFactors = FALSE)
  stock <- data.frame(stringsAsFactors = FALSE)
  problems <- data.frame(stringsAsFactors = FALSE)
  used_specimens <- c()

  if(!missing(exclude)) {
    exclude_list <- create_exclude_list(.dots = exclude)
  } else {
    exclude_list <- c()
  }

  add_problem <- function(problems, request_id, problem, level = "moderate") {
    rbind(problems, list(REQUEST_ID = request_id, DESCRIPTION = problem, LEVEL = level), stringsAsFactors = FALSE)
  }

  sizes = mmge::get_protocol_setting("aliquot_sizes")

  nucleic_acid_ranges <- list(
    DNA = mmge::get_protocol_setting("dna_settings")$aliquot_range,
    RNA = mmge::get_protocol_setting("rna_settings")$aliquot_range
  )

  dna_remainder <- mmge::get_protocol_setting("dna_settings")$min_remainder
  dep_levels <- mmge::get_protocol_setting("depletion_warning_levels")

  rp <- grepl("POOL|Pool", names(ids), fixed = FALSE)

  ref_pools <- ids[rp]
  ids <- ids[!rp]

  if(length(ids) > 0) {

    specimen_request <- as.data.frame(do.call(rbind, regmatches(
      names(ids),
      regexec("^([0-9]+)_?([0-9]+)?$", names(ids))
    )), stringsAsFactors = FALSE)
    colnames(specimen_request) <- c("SAMPLE_ID", "SPECIMEN_ID", "SIZE")
    specimen_request$SIZE <- as.numeric(specimen_request$SIZE)
    specimen_request$SIZE[is.na(specimen_request$SIZE)] <- 1
    specimen_request$COUNT <- unlist(ids[specimen_request$SAMPLE_ID == names(ids)])

    specimen_query <- oncore2::oncore_connect(verbose = verbose, query_name = sprintf("%s_pullsheet", api_name)) %>%
      oncore2::select_fields(SPECIMEN_NO, SPECIMEN_STORAGE_LOCATION, PARENT_SPECIMEN_ID,
                    SUBJECT_LAST_NAME, COLLECTION_GROUP, SPECIMEN_TYPE, VISIT,
                    SPECIMEN_QUANTITY, UNIT_OF_MEASURE, SPECIMEN_BAR_CODE,
                    SPECIMEN_COMMENTS, PCS_SPECIMEN_ID, SPECIMEN_STATUS) %>%
      oncore2::filter_oncore(oncore2::protocol(toupper(api_name))) %>%
      oncore2::execute_query() %>%
      dplyr::group_by(SPECIMEN_NO) %>%
      dplyr::mutate(SPECIMEN_ID = regmatches(SPECIMEN_NO, regexec("^[0-9]+", SPECIMEN_NO))[[1]]) %>%
      dplyr::ungroup() %>%
      dplyr::mutate(COLLECTION_GROUP = ifelse(COLLECTION_GROUP == "BUFFY COAT", "DNA", COLLECTION_GROUP)) %>%
      dplyr::filter(!SPECIMEN_NO %in% exclude_list)

    parent_ids <- unique(specimen_query$PARENT_SPECIMEN_ID)

    specimen_lookup <- specimen_query %>%
      mutate(SPECIMEN_ID = unlist(regmatches(SPECIMEN_NO, regexec("^[0-9]+", SPECIMEN_NO)))) %>%
      distinct(SPECIMEN_ID, SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP)

    specimen_query <- specimen_query %>%
      filter(SPECIMEN_STATUS == "Available")

    specimen_request <- specimen_request %>%
      left_join(specimen_lookup)

    specimens_unknown <- specimen_request %>%
      filter(is.na(SUBJECT_LAST_NAME))

    if(nrow(specimens_unknown) > 0) {
      for(i in seq(nrow(specimens_unknown))) {
        problems <- problems %>% add_problem(specimens_unknown$SAMPLE_ID[i], "Unable to locate requested sample in OnCore.", "severe")
      }
    }

    specimen_supply <- specimen_query %>%
      filter(SPECIMEN_STATUS == "Available") %>%
      right_join(specimen_request, by = c("SUBJECT_LAST_NAME", "VISIT", "COLLECTION_GROUP")) %>%
      filter((!grepl(".", SPECIMEN_NO, fixed = TRUE) & !(PCS_SPECIMEN_ID %in% parent_ids)) | SPECIMEN_QUANTITY %in% sizes[COLLECTION_GROUP]) %>%
      group_by(SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP, SPECIMEN_TYPE, UNIT_OF_MEASURE) %>%
      summarize(
        VOLUME = sum(ifelse(UNIT_OF_MEASURE == "ul", SPECIMEN_QUANTITY, 0)),
        MASS = sum(ifelse(UNIT_OF_MEASURE == "ug", SPECIMEN_QUANTITY, 0))
      )

    specimen_supply <- specimen_request %>%
      mutate(REQUESTED_VOLUME = COUNT * SIZE) %>%
      left_join(specimen_supply, by = c("SUBJECT_LAST_NAME", "VISIT", "COLLECTION_GROUP")) %>%
      mutate(REMAINING = ifelse(COLLECTION_GROUP %in% c("DNA", "RNA"), MASS - REQUESTED_VOLUME, VOLUME - REQUESTED_VOLUME))

    specimen_depletion <- specimen_supply %>%
      filter(REMAINING <= dep_levels[COLLECTION_GROUP])

    if(nrow(specimen_depletion) > 0) {
      for(i in seq(nrow(specimen_depletion))) {
        problems <- problems %>%
          add_problem(specimen_depletion$SAMPLE_ID[i], sprintf("Depletion Warning: %s%s will remain.", specimen_depletion$REMAINING[i], specimen_depletion$UNIT_OF_MEASURE[i]), "high")
      }
    }

    specimen_list <- specimen_query %>%
      filter(!SPECIMEN_NO %in% used_specimens) %>%
      left_join(specimen_request) %>%
      filter(!is.na(SAMPLE_ID)) %>%
      filter(SPECIMEN_QUANTITY == SIZE) %>%
      group_by(SPECIMEN_ID) %>%
      filter(row_number() <= unique(COUNT))

    specimens_missing <- specimen_counts(specimen_list, specimen_request) %>%
      filter(MISSING > 0)

    if(nrow(specimens_missing) > 0) {
      cousins_list <- specimen_query %>%
        anti_join(specimen_list, by = "SPECIMEN_NO") %>%
        rename(NEW_SPECIMEN_ID = SPECIMEN_ID) %>%
        right_join(specimens_missing %>% rename(COUNT = REQUESTED), by = c("SUBJECT_LAST_NAME", "VISIT", "COLLECTION_GROUP")) %>%
        filter(SPECIMEN_QUANTITY == SIZE) %>%
        group_by(SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP) %>%
        filter(row_number() <= unique(MISSING)) %>%
        mutate(SPECIMEN_COMMENTS = gsub("^NA - ", "", sprintf("%s - PULLSHEET_COMMENT: %s", SPECIMEN_COMMENTS, "COUSIN ALIQUOT"))) %>%
        select_(.dots = colnames(specimen_list))
      pull <- rbind(specimen_list, cousins_list)
      for(i in seq(nrow(cousins_list))) {
        problems <- problems %>%
          add_problem(cousins_list$SAMPLE_ID[i], sprintf("Cousin Aliquot Chosen: %s", cousins_list$SPECIMEN_NO[i]), level = "low")
      }
    } else {
      pull <- specimen_list
    }

    specimens_missing <- specimen_counts(pull, specimen_request) %>%
      filter(MISSING > 0)

    if(nrow(specimens_missing) > 0) {

      stock <- specimen_query %>%
        filter(!SPECIMEN_NO %in% used_specimens) %>%
        rename(NEW_SPECIMEN_ID = SPECIMEN_ID) %>%
        right_join(specimens_missing %>% rename(COUNT = REQUESTED), by = c("SUBJECT_LAST_NAME", "VISIT", "COLLECTION_GROUP")) %>%
        filter(!(grepl(".", SPECIMEN_NO, fixed = TRUE)) & !(PCS_SPECIMEN_ID %in% parent_ids)) %>%
        group_by(SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP) %>%
        filter(row_number() == 1) %>%
        mutate(SPECIMEN_COMMENTS = gsub("^NA - ", "", sprintf("%s - PULLSHEET_COMMENT: %s", SPECIMEN_COMMENTS, "NEW PARENT CHOSEN"))) %>%
        select_(.dots = colnames(specimen_list)) %>%
        select(SPECIMEN_NO,
               SPECIMEN_STORAGE_LOCATION,
               SPECIMEN_BAR_CODE,
               SUBJECT_LAST_NAME,
               VISIT,
               COLLECTION_GROUP,
               SPECIMEN_TYPE,
               SPECIMEN_QUANTITY,
               UNIT_OF_MEASURE,
               SPECIMEN_COMMENTS,
               REQUEST_ID = SAMPLE_ID,
               REQUESTED_COUNT = COUNT)

    }

  }

  if(length(ref_pools) > 0) {

    pool_request <- data.frame(POOL_ID = names(ref_pools), COUNT = unlist(ref_pools), stringsAsFactors = FALSE)

    pool_query <- oncore2::oncore_connect(verbose = verbose, query_name = sprintf("%s_refpool_pullsheet", api_name)) %>%
      oncore2::select_fields(SPECIMEN_NO, SPECIMEN_STORAGE_LOCATION, PARENT_SPECIMEN_ID, CASE_NO,
                    SUBJECT_LAST_NAME, COLLECTION_GROUP, SPECIMEN_TYPE, VISIT,
                    SPECIMEN_QUANTITY, UNIT_OF_MEASURE, SPECIMEN_BAR_CODE,
                    SPECIMEN_COMMENTS, PCS_SPECIMEN_ID, SPECIMEN_STATUS) %>%
      oncore2::filter_oncore(oncore2::protocol("REF-POOL"), oncore2::specimen_status("Available")) %>%
      oncore2::execute_query(recode = TRUE) %>%
      dplyr::filter(!SPECIMEN_NO %in% exclude_list)

    pool_pull <- pool_query %>%
      filter(!SPECIMEN_NO %in% used_specimens) %>%
      filter(CASE_NO %in% pool_request$POOL_ID) %>%
      select(-SPECIMEN_STORAGE_LOCATION) %>%
      left_join(pool_query %>% select(SPECIMEN_NO, SPECIMEN_STORAGE_LOCATION))

    pool_pull <- pool_query %>%
      filter(!SPECIMEN_NO %in% used_specimens) %>%
#      filter(CASE_NO %in% pool_request$POOL_ID) %>%
      left_join(pool_request, by = c(CASE_NO = "POOL_ID")) %>%
      group_by(CASE_NO) %>%
      filter(row_number() <= unique(COUNT)) %>%
      rename(SPECIMEN_ID = CASE_NO) %>%
      mutate(SAMPLE_ID = SPECIMEN_ID) %>%
      mutate(SIZE = SPECIMEN_QUANTITY)

    if(sum(pool_request$COUNT) > nrow(pool_pull)) {
      problems <- problems %>% add_problem("REF_POOLS", "Insufficient Reference Pools to fulfill request.", "severe")
    }

    pool_request <- pool_request %>%
      left_join(
        pool_query %>%
          mutate(SPECIMEN_ID = CASE_NO) %>%
          distinct(SPECIMEN_ID, SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP, SPECIMEN_QUANTITY),
        by = c(POOL_ID = "SPECIMEN_ID")
      ) %>%
      mutate(SAMPLE_ID = POOL_ID) %>%
      select(SAMPLE_ID, SPECIMEN_ID = POOL_ID, SIZE = SPECIMEN_QUANTITY, COUNT, SUBJECT_LAST_NAME, VISIT, COLLECTION_GROUP)

    if(nrow(pull) > 0) {
      pull <- rbind(pull, pool_pull %>% select_(.dots = colnames(pull))) %>%
        ungroup()
    } else {
      pull <- pool_pull
    }

    pull <- pull %>%
      select(SPECIMEN_NO,
             SPECIMEN_STORAGE_LOCATION,
             SPECIMEN_BAR_CODE,
             SUBJECT_LAST_NAME,
             VISIT,
             COLLECTION_GROUP,
             SPECIMEN_TYPE,
             SPECIMEN_QUANTITY,
             UNIT_OF_MEASURE,
             SPECIMEN_COMMENTS,
             REQUEST_ID = SAMPLE_ID,
             REQUESTED_COUNT = COUNT)

  }

  if(exists("specimen_request") & exists("pool_request")) {
    request <- rbind(specimen_request, pool_request)
  } else if(exists("specimen_request")) {
    request <- specimen_request
  } else {
    request <- pool_request
  }


  pullsheet <- list()

  if(nrow(pull) > 0) {
    pullsheet$`To Pull` <- mmgePullsheets:::select_pullsheet_fields_(as.data.frame(pull), subject_id = "SUBJECT_LAST_NAME")
  }

  if(nrow(stock) > 0) {
    pullsheet$`To Aliquot` <- mmgePullsheets:::select_pullsheet_fields_(as.data.frame(stock), subject_id = "SUBJECT_LAST_NAME")
  }

  pullsheet$`Original Request` <- request

  if(exists("specimen_supply")) {
    pullsheet$`Supply` <- as.data.frame(specimen_supply, stringsAsFactors = FALSE)
  }

  if(nrow(problems) > 0) {
    pullsheet$`Problems` <- as.data.frame(problems, stringsAsFactors = FALSE)
  }

  class(pullsheet) <- c("pullsheet", class(pullsheet))

  return(pullsheet)

}

specimen_counts <- function(specimen_list, specimen_request) {
  specimen_request %>%
    left_join(
      specimen_list %>%
        group_by(SAMPLE_ID) %>%
        summarize(
          REQUESTED = unique(COUNT),
          AVAILABLE = n(),
          MISSING = REQUESTED - AVAILABLE
        )
    ) %>%
    mutate(REQUESTED = ifelse(is.na(REQUESTED), COUNT, REQUESTED)) %>%
    mutate(AVAILABLE = ifelse(is.na(AVAILABLE), 0, AVAILABLE)) %>%
    mutate(MISSING = ifelse(is.na(MISSING), REQUESTED - AVAILABLE, MISSING))
}
