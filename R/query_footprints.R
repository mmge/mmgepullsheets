#'Query the footprints RESTful API
#'
#'@param \dots named arguments to search footprints by
#'
#'@export
query_footprints <- function(...) {

  url <- "https://ebidvt.uits.iu.edu/denodo-restfulws/footprints/views/fpprod_in_medgen_issue"

  parameters <- list(...)

  if(length(parameters) > 0) {

    parameters <- lapply(parameters, function(p) {
      URLencode(paste0("'", paste(p, collapse = "|"), "'"), reserved = TRUE)
    })

    parameters <- paste(names(parameters), URLencode(parameters), sep = "+regexp_ilike+", collapse = "+AND+")

    filter <- sprintf("$filter=%s", parameters)

    url <- sprintf("%s?%s", url, filter)

  }

  x <- httr::content(httr::GET(url, httr::accept_json(), mmge::authenticate_iu()))$elements

  x <- as.data.frame(do.call(rbind, x))

  return(x)

}