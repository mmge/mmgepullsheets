select_box2 <- function(box_design, i) {

  boxes <- box_design$box_stats
  boxes$KEEP <- FALSE
  for(bv in names(box_design$balance_vars)) {
    boxes$KEEP[boxes$group == bv & boxes$variable == box_design$subjects[i, bv][[1]]] <- TRUE
  }
  boxes <- boxes[boxes$KEEP, ] %>%
    group_by(BOX_NO) %>%
    summarize(
      SCORE = sum(SCORE),
      NEGATIVE = sum(SCORE[SCORE < 0])
    ) %>%
    left_join(calculate_box_fullness(box_design), by = "BOX_NO")

  b <- boxes[boxes$EMPTY >= box_design$subjects$VISITS[i] & boxes$SCORE > 0 & boxes$BOX_NO != max(boxes$BOX_NO), ]

  if(nrow(b) == 1) {
    b <- b$BOX_NO[1]
  } else if(nrow(b) > 1) {
    b <- sample(b$BOX_NO, 1, prob = 1/b$SCORE^2)
  } else if(nrow(b) == 0) {
    b <- boxes[boxes$EMPTY >= box_design$subjects$VISITS[i] & boxes$NEGATIVE == max(boxes$NEGATIVE) & boxes$BOX_NO != max(boxes$BOX_NO), ]
    if(nrow(b) == 1) {
      b <- b$BOX_NO[1]
    } else if(nrow(b) > 1) {
      b <- sample(b$BOX_NO, 1, prob = b$SCORE)
    } else if(boxes$EMPTY[boxes$BOX_NO == max(boxes$BOX_NO)] >= box_design$subjects$VISITS[i]) {
      b <- max(boxes$BOX_NO)
    } else {
      stop("Can't create the box design...")
    }
  } else {
    stop("I don't know how nrow(b) could be negative...")
  }

  return(b)
}